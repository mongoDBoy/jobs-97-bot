"""
This module creates DB collections and set their pattern
"""

from mongoengine import (
    PULL,
    BooleanField,
    Document,
    IntField,
    ListField,
    LongField,
    ReferenceField,
    StringField,
)


class Card(Document):
    """
    Class that creates DB "cards" collection

    Collection params:
    :param value: card value (can be "supporter" or "opponent")
    """

    value = StringField(required=True)

    meta = {"collection": "cards"}


class Vote(Document):
    """
    Class that creates DB "votes" collection

    Collection params:
    :param player_id: voted player's id
    :param server_id: voted player's server id
    :param value: the vote itself (bool)
    """

    player_id = LongField(required=True, unique=True)
    server_id = LongField(required=True)
    value = BooleanField(required=True)

    meta = {"collection": "votes"}


class Player(Document):
    """
    Class that creates DB "players" collection

    :param id: player's discord ID
    :param alias: player's discord nickname
    :param character: player's character (can be "ceo-candidate", "ceo" or "vp")
    :param role: player's role (can be "supporter", "opponent" or "steve-jobs")
    """

    id = LongField(primary_key=True, required=True)
    alias = StringField(required=True)
    character = StringField()
    role = StringField()

    meta = {"collection": "players"}


class Session(Document):
    """
    Class that creates DB "sessions" collection

    :param id: discord server ID
    :param players: list of Player objects that belong to session
    :param state: session state (can be "vp-candidate-choice",
               "ceo-candidate-voting", "vp-card-choice" or "ceo-card-choice")
    :param rounds_amount: session round number
    :param cards: list of Card objects that belong to session
    :param supporters_card_amount: the amount of supporters cards accepted
    :param opponents_card_amount: the amount of opponents cards accepted
    """

    id = LongField(primary_key=True, required=True)
    players = ListField(ReferenceField(Player, reverse_delete_rule=PULL))
    state = StringField()
    rounds_amount = IntField(default=0)
    cards = ListField(ReferenceField(Card, reverse_delete_rule=PULL))
    supporters_card_amount = IntField(default=0)
    opponents_card_amount = IntField(default=0)

    meta = {"collection": "sessions"}
