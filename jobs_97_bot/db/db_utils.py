"""
This module provides useful functions that work with DB, all that functions
can be used in any of the other project files
"""

import random

import discord

from jobs_97_bot.config import (
    CATEGORY_NAME,
    MIN_PLAYER_AMOUNT,
    TEXT_CHANNEL_NAME,
    VOICE_CHANNEL_NAME,
)
from jobs_97_bot.db.models import Card, Player, Session, Vote


def add_session(server_id: int):
    Session(id=server_id).save()


def remove_session(server_id: int):
    session = Session.objects(id=server_id).first()
    if session:
        clear_players_list(server_id)
        delete_all_cards(server_id)
        remove_votes(server_id)
        session.delete()


def reset_session(server_id: int):
    remove_session(server_id)
    add_session(server_id)


def get_players(server_id: int):
    session = Session.objects(id=server_id).first()
    return session.players


def get_player_by_id(player_id: int):
    return Player.objects(id=player_id).first()


def get_player_by_alias(player_alias: str):
    return Player.objects(alias=player_alias).first()


def get_players_id_list(server_id: int):
    players = get_players(server_id)
    return list(map(lambda x: x.id, players))


def get_players_alias_list(server_id: int):
    players = get_players(server_id)
    return list(map(lambda x: x.alias, players))


def add_player(server_id: int, player_id: int, player_alias: str):
    if not get_player_by_id(player_id):
        player = Player(id=player_id, alias=player_alias)
        player.save()
        session = Session.objects(id=server_id).first()
        session.players.append(player)
        session.save()


def remove_player(player_id: int):
    Player.objects(id=player_id).delete()


def clear_players_list(server_id: int):
    players_id_list = get_players_id_list(server_id)
    for player_id in players_id_list:
        Player.objects(id=player_id).delete()


def check_if_in_session(server_id: int, player_id: int):
    return player_id in get_players_id_list(server_id)


def check_if_session_is_running(server_id: int):
    if get_state(server_id):
        return True
    return False


def get_state(server_id: int):
    session = Session.objects(id=server_id).first()
    return session.state


def set_state(server_id: int, new_state: str):
    session = Session.objects(id=server_id).first()
    session.state = new_state
    session.save()


def set_roles(server_id: int):
    players_id_list = get_players_id_list(server_id)
    random.shuffle(players_id_list)
    supporter_amount = (len(players_id_list) - MIN_PLAYER_AMOUNT) // 2 + 2

    player = get_player_by_id(players_id_list[0])
    player.role = "steve-jobs"
    player.save()

    for i in range(1, len(players_id_list)):
        player = get_player_by_id(players_id_list[i])
        role = "supporter" if i < supporter_amount else "opponent"
        player.role = role
        player.save()


def set_ceo_candidate(player_id: int):
    player = get_player_by_id(player_id)
    player.character = "ceo-candidate"
    player.save()


def remove_ceo_candidate(server_id: int):
    player = get_ceo_candidate(server_id)
    if player:
        player.character = ""
        player.save()


def get_ceo_candidate(server_id: int):
    players = get_players(server_id)
    lst = list(filter(lambda x: x.character == "ceo-candidate", players))
    if lst:
        return lst[0]
    return None


def get_ceo(server_id: int):
    players = get_players(server_id)
    lst = list(filter(lambda x: x.character == "ceo", players))
    if lst:
        return lst[0]
    return None


def set_ceo(player_id: int):
    player = get_player_by_id(player_id)
    player.character = "ceo"
    player.save()


def remove_ceo(server_id: int):
    player = get_ceo(server_id)
    if player:
        player.character = ""
        player.save()


def update_rounds_amount(server_id: int):
    session = Session.objects(id=server_id).first()
    session.rounds_amount += 1
    session.save()


def set_vp(server_id: int):
    session = Session.objects(id=server_id).first()
    player = session.players[session.rounds_amount % len(session.players)]
    player.character = "vp"
    player.save()


def get_vp(server_id: int):
    players = get_players(server_id)
    lst = list(filter(lambda x: x.character == "vp", players))
    if lst:
        return lst[0]
    return None


def remove_vp(server_id: int):
    player = get_vp(server_id)
    if player:
        player.character = ""
        player.save()


def get_steve_jobs(server_id: int):
    players = get_players(server_id)
    return list(filter(lambda x: x.role == "steve-jobs", players))[0]


def get_supporters(server_id: int):
    players = get_players(server_id)
    return list(filter(lambda x: x.role == "supporter", players))


def get_opponents(server_id: int):
    players = get_players(server_id)
    return list(filter(lambda x: x.role == "opponent", players))


def set_vote(server_id: int, player_id: int, value: bool):
    Vote(server_id=server_id, player_id=player_id, value=value).save()


def get_votes(server_id: int):
    return Vote.objects(server_id=server_id)


def get_votes_with_yes_ans(server_id: int):
    return Vote.objects(server_id=server_id, value=True)


def get_votes_with_no_ans(server_id: int):
    return Vote.objects(server_id=server_id, value=False)


def remove_votes(server_id: int):
    Vote.objects(server_id=server_id).delete()


def add_random_card(server_id: int):
    card = Card(value=random.choice(["supporter", "opponent"]))
    card.save()
    session = Session.objects(id=server_id).first()
    session.cards.append(card)
    session.save()


def get_card(server_id: int, index: int):
    cards = get_cards(server_id)
    if index < len(cards):
        return cards[index]
    return None


def get_cards(server_id: int):
    session = Session.objects(id=server_id).first()
    return session.cards


def delete_card(server_id: int, index: int):
    card = get_card(server_id, index)
    if card:
        card.delete()


def delete_all_cards(server_id: int):
    session = Session.objects(id=server_id).first()
    for card in session.cards:
        card.delete()


def get_server_by_player_id(player_id: int):
    sessions = Session.objects()
    for session in sessions:
        if player_id in get_players_id_list(session.id):
            return session
    return None


def update_supporters_card_amount(server_id: int):
    session = Session.objects(id=server_id).first()
    session.supporters_card_amount += 1
    session.save()


def reset_supporters_card_amount(server_id: int):
    session = Session.objects(id=server_id).first()
    session.supporters_card_amount = 0
    session.save()


def get_supporters_card_amount(server_id: int):
    session = Session.objects(id=server_id).first()
    return session.supporters_card_amount


def reset_opponents_card_amount(server_id: int):
    session = Session.objects(id=server_id).first()
    session.opponents_card_amount = 0
    session.save()


def update_opponents_card_amount(server_id: int):
    session = Session.objects(id=server_id).first()
    session.opponents_card_amount += 1
    session.save()


def get_opponents_card_amount(server_id: int):
    session = Session.objects(id=server_id).first()
    return session.opponents_card_amount


async def create_channel(guild):
    category = await guild.create_category(CATEGORY_NAME)
    await guild.create_voice_channel(VOICE_CHANNEL_NAME, category=category)
    return await guild.create_text_channel(
        TEXT_CHANNEL_NAME, category=category
    )


async def get_channel(guild):
    channel = discord.utils.get(guild.text_channels, name=TEXT_CHANNEL_NAME)
    if channel:
        return channel
    return await create_channel(guild)
