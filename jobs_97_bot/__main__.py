"""
This module runs the bot
"""

import jobs_97_bot.config as config
from jobs_97_bot.initialization import bot


def main():
    bot.run(config.DISCORD_TOKEN)


if __name__ == "__main__":
    main()
