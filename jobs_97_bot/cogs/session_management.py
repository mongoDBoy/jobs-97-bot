"""
This module processes bot commands to manage the session
"""

from discord.ext import commands

from jobs_97_bot.cogs.round_processing import start_round
from jobs_97_bot.config import (
    MAX_PLAYER_AMOUNT,
    MIN_PLAYER_AMOUNT,
    TEXT_CHANNEL_NAME,
)
from jobs_97_bot.db.db_utils import (
    add_player,
    check_if_in_session,
    check_if_session_is_running,
    clear_players_list,
    get_opponents,
    get_players,
    get_players_id_list,
    get_steve_jobs,
    get_supporters,
    remove_player,
    reset_opponents_card_amount,
    reset_session,
    reset_supporters_card_amount,
    set_roles,
)
from jobs_97_bot.utils.strings import (
    DM_ASSIGN_ROLE_OPPONENT,
    DM_ASSIGN_ROLE_STEVE_JOBS,
    DM_ASSIGN_ROLE_SUPPORTER,
    DM_ON_SERVER_CLEAR_COMMAND_TEXT,
    DM_ON_SERVER_JOIN_COMMAND_TEXT,
    DM_ON_SERVER_LEAVE_COMMAND_TEXT,
    ON_SERVER_COMMAND_VIA_DM_TEXT,
    SERVER_END_COMMAND_TEXT,
    SERVER_ON_WRONG_CHANNEL_TEXT,
    SERVER_PLAYERS_COMMAND,
    SERVER_START_COMMAND,
)
from jobs_97_bot.utils.utils import get_embed, send_dm


async def set_session(bot, ctx):
    server = ctx.guild
    players = get_players(server.id)

    if len(players) < MIN_PLAYER_AMOUNT:
        await ctx.send(
            SERVER_START_COMMAND["min_amount_error_text"].format(
                min_player_amount=MIN_PLAYER_AMOUNT
            )
        )
    elif len(players) > MAX_PLAYER_AMOUNT:
        await ctx.send(
            SERVER_START_COMMAND["max_amount_error_text"].format(
                max_player_amount=MAX_PLAYER_AMOUNT
            )
        )
    else:
        await ctx.send(
            embed=get_embed(
                title=SERVER_START_COMMAND["title"],
                description=SERVER_START_COMMAND["text"],
                footer=SERVER_START_COMMAND["footer"],
            )
        )

        reset_supporters_card_amount(server.id)
        reset_opponents_card_amount(server.id)
        set_roles(server.id)

        steve_jobs = get_steve_jobs(server.id)
        supporters = get_supporters(server.id)
        opponents = get_opponents(server.id)

        await send_dm(
            bot=bot,
            players_id_list=[steve_jobs.id],
            title=DM_ASSIGN_ROLE_STEVE_JOBS["title"],
            description=DM_ASSIGN_ROLE_STEVE_JOBS["text"].format(
                supporters=", ".join([player.alias for player in supporters])
            ),
            footer=DM_ASSIGN_ROLE_STEVE_JOBS["footer"],
        )

        await send_dm(
            bot=bot,
            players_id_list=[player.id for player in supporters],
            title=DM_ASSIGN_ROLE_SUPPORTER["title"],
            description=DM_ASSIGN_ROLE_SUPPORTER["text"].format(
                supporters=", ".join([player.alias for player in supporters]),
                steve_jobs=steve_jobs.alias,
            ),
            footer=DM_ASSIGN_ROLE_SUPPORTER["footer"],
        )

        await send_dm(
            bot=bot,
            players_id_list=[player.id for player in opponents],
            title=DM_ASSIGN_ROLE_OPPONENT["title"],
            description=DM_ASSIGN_ROLE_OPPONENT["text"],
            footer=DM_ASSIGN_ROLE_OPPONENT["footer"],
        )

        await start_round(bot, server)


class SessionManagement(commands.Cog):
    """
    Class that processes bot commands to manage the session

    List of commands to process:

    players - shows current session players list
    join - adds player, who wrote command, to session players list
    leave - deletes player, who wrote command, from session players list
    clear - resets session players list (delete every player from it)
    start - starts the session
    end - terminates the session
    """

    def __init__(self, bot):
        self.bot = bot

    @commands.command()
    async def players(self, ctx):
        if ctx.guild:
            if ctx.message.channel.name == TEXT_CHANNEL_NAME:
                server = ctx.guild
                players_mention_list = list(
                    map(lambda x: f"<@{x}>", get_players_id_list(server.id))
                )
                await ctx.send(
                    embed=get_embed(
                        title=SERVER_PLAYERS_COMMAND["title"],
                        description="\n".join(players_mention_list),
                        footer=SERVER_PLAYERS_COMMAND["footer"].format(
                            players_amount=len(get_players(server.id))
                        ),
                    )
                )
            else:
                await ctx.send(SERVER_ON_WRONG_CHANNEL_TEXT)
        else:
            await ctx.send(ON_SERVER_COMMAND_VIA_DM_TEXT)

    @commands.command()
    async def join(self, ctx):
        if ctx.guild:
            if ctx.message.channel.name == TEXT_CHANNEL_NAME:
                server, msg_author = ctx.guild, ctx.message.author
                if not check_if_in_session(server.id, msg_author.id):
                    add_player(
                        server.id, msg_author.id, msg_author.display_name
                    )
                    await msg_author.send(
                        DM_ON_SERVER_JOIN_COMMAND_TEXT.format(
                            server_name=server.name
                        )
                    )
            else:
                await ctx.send(SERVER_ON_WRONG_CHANNEL_TEXT)
        else:
            await ctx.send(ON_SERVER_COMMAND_VIA_DM_TEXT)

    @commands.command()
    async def leave(self, ctx):
        if ctx.guild:
            if ctx.message.channel.name == TEXT_CHANNEL_NAME:
                server, msg_author = ctx.guild, ctx.message.author
                if check_if_in_session(server.id, msg_author.id):
                    remove_player(msg_author.id)
                    await msg_author.send(
                        DM_ON_SERVER_LEAVE_COMMAND_TEXT.format(
                            server_name=server.name
                        )
                    )
            else:
                await ctx.send(SERVER_ON_WRONG_CHANNEL_TEXT)
        else:
            await ctx.send(ON_SERVER_COMMAND_VIA_DM_TEXT)

    @commands.command()
    async def clear(self, ctx):
        if ctx.guild:
            if ctx.message.channel.name == TEXT_CHANNEL_NAME:
                server = ctx.guild
                players_id_list = get_players_id_list(server.id)
                await send_dm(
                    bot=self.bot,
                    players_id_list=players_id_list,
                    text=DM_ON_SERVER_CLEAR_COMMAND_TEXT.format(
                        server_name=server.name
                    ),
                )
                clear_players_list(server.id)
            else:
                await ctx.send(SERVER_ON_WRONG_CHANNEL_TEXT)
        else:
            await ctx.send(ON_SERVER_COMMAND_VIA_DM_TEXT)

    @commands.command()
    async def start(self, ctx):
        if ctx.guild:
            if ctx.message.channel.name == TEXT_CHANNEL_NAME:
                server = ctx.guild
                if not check_if_session_is_running(server.id):
                    await set_session(self.bot, ctx)
            else:
                await ctx.send(SERVER_ON_WRONG_CHANNEL_TEXT)
        else:
            await ctx.send(ON_SERVER_COMMAND_VIA_DM_TEXT)

    @commands.command()
    async def end(self, ctx):
        if ctx.guild:
            if ctx.message.channel.name == TEXT_CHANNEL_NAME:
                server = ctx.guild
                if check_if_session_is_running(server.id):
                    reset_session(server.id)
                    await ctx.send(SERVER_END_COMMAND_TEXT)
            else:
                await ctx.send(SERVER_ON_WRONG_CHANNEL_TEXT)
        else:
            await ctx.send(ON_SERVER_COMMAND_VIA_DM_TEXT)
