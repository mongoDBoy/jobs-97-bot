"""
This module processes bot commands while game session is running:
"""

import os

import discord
from discord.ext import commands

from jobs_97_bot.config import (
    APP_FOLDER_NAME,
    CARD_COMBINATIONS_FOLDER_NAME,
    CEO_CANDIDATE_VOTING_STATE,
    CEO_CARD_CHOICE,
    IMG_FOLDER_NAME,
    TEXT_CHANNEL_NAME,
    VP_CARD_CHOICE,
    VP_CEO_CHOICE_STATE,
)
from jobs_97_bot.db.db_utils import (
    add_random_card,
    delete_all_cards,
    delete_card,
    get_card,
    get_ceo,
    get_ceo_candidate,
    get_channel,
    get_opponents_card_amount,
    get_players_id_list,
    get_server_by_player_id,
    get_state,
    get_steve_jobs,
    get_supporters_card_amount,
    get_votes,
    get_votes_with_no_ans,
    get_votes_with_yes_ans,
    get_vp,
    remove_ceo,
    remove_ceo_candidate,
    remove_votes,
    remove_vp,
    set_ceo,
    set_ceo_candidate,
    set_state,
    set_vote,
    set_vp,
    update_opponents_card_amount,
    update_rounds_amount,
    update_supporters_card_amount,
)
from jobs_97_bot.utils.strings import (
    DM_ON_CEO_ASSIGNMENT_SUCCESS,
    DM_ON_CHOOSE_CARD_COMMAND,
    DM_ON_SERVER_YES_COMMAND_TEXT,
    DM_ON_VP_CHOOSE_CARD_COMMAND_SUCCESS,
    SERVER_CHOOSE_CANDIDATE_COMMAND,
    SERVER_ON_CEO_ASSIGNMENT_FAILURE,
    SERVER_ON_CEO_ASSIGNMENT_SUCCESS,
    SERVER_ON_CEO_CHOOSE_CARD_COMMAND_SUCCESS,
    SERVER_ON_OPPONENTS_WIN,
    SERVER_ON_SUPPORTERS_WIN,
    SERVER_ON_VP_ASSIGNMENT,
    SERVER_ON_VP_CHOOSE_CARD_COMMAND_SUCCESS,
    SERVER_ON_WRONG_CHANNEL_TEXT,
)
from jobs_97_bot.utils.utils import (
    get_embed,
    get_id_from_discord_mention,
    get_index,
    send_dm,
)


async def start_round(bot, server):
    remove_vp(server.id)
    remove_ceo_candidate(server.id)
    remove_ceo(server.id)
    remove_votes(server.id)
    delete_all_cards(server.id)

    set_vp(server.id)
    update_rounds_amount(server.id)
    set_state(server.id, VP_CEO_CHOICE_STATE)

    vp = get_vp(server.id)
    current_guild = bot.get_guild(server.id)
    channel = await get_channel(current_guild)
    await channel.send(
        embed=get_embed(
            title=SERVER_ON_VP_ASSIGNMENT["title"],
            description=SERVER_ON_VP_ASSIGNMENT["text"].format(
                mention=f"<@{vp.id}>"
            ),
            footer=SERVER_ON_VP_ASSIGNMENT["footer"],
            server_id=server.id,
        )
    )


async def end_round(bot, server, chosen_card_let: str):
    if chosen_card_let == "s":
        update_supporters_card_amount(server.id)
    elif chosen_card_let == "o":
        update_opponents_card_amount(server.id)

    set_state(server.id, "")
    current_guild = bot.get_guild(server.id)
    channel = await get_channel(current_guild)

    supporters_card_amount = get_supporters_card_amount(server.id)
    opponents_card_amount = get_opponents_card_amount(server.id)
    ceo = get_ceo(server.id)
    steve_jobs = get_steve_jobs(server.id)

    if supporters_card_amount == 3 and ceo.id == steve_jobs.id:
        await channel.send(
            embed=get_embed(
                title=SERVER_ON_SUPPORTERS_WIN["with_3_cards"]["title"],
                description=SERVER_ON_SUPPORTERS_WIN["with_3_cards"]["text"],
                server_id=server.id,
            )
        )
    elif supporters_card_amount == 6:
        await channel.send(
            embed=get_embed(
                title=SERVER_ON_SUPPORTERS_WIN["with_6_cards"]["title"],
                description=SERVER_ON_SUPPORTERS_WIN["with_6_cards"]["text"],
                server_id=server.id,
            )
        )
    elif opponents_card_amount == 5:
        await channel.send(
            embed=get_embed(
                title=SERVER_ON_OPPONENTS_WIN["title"],
                description=SERVER_ON_OPPONENTS_WIN["text"],
                server_id=server.id,
            )
        )
    else:
        await start_round(bot, server)


async def process_vote_ans(ctx, bot, ans):
    server, msg_author = ctx.guild, ctx.message.author
    votes, players = get_votes(server.id), get_players_id_list(server.id)

    if get_state(server.id) == CEO_CANDIDATE_VOTING_STATE:
        if msg_author.id not in [vote.player_id for vote in votes]:
            set_vote(server.id, msg_author.id, ans)

            await send_dm(
                bot=bot,
                players_id_list=[msg_author.id],
                text=DM_ON_SERVER_YES_COMMAND_TEXT.format(
                    server_name=server.name
                ),
            )

            if len(votes) == len(players) - 1:
                yes_ans_amount = len(get_votes_with_yes_ans(server.id))
                no_ans_amount = len(get_votes_with_no_ans(server.id))
                ceo_candidate = get_ceo_candidate(server.id)

                if yes_ans_amount > no_ans_amount:
                    supporters_card_amount = get_supporters_card_amount(
                        server.id
                    )
                    set_ceo(ceo_candidate.id)
                    ceo = get_ceo(server.id)
                    steve_jobs = get_steve_jobs(server.id)

                    if supporters_card_amount == 3 and ceo.id == steve_jobs.id:
                        set_state(server.id, "")
                        await ctx.send(
                            embed=get_embed(
                                title=SERVER_ON_SUPPORTERS_WIN["with_3_cards"][
                                    "title"
                                ],
                                description=SERVER_ON_SUPPORTERS_WIN[
                                    "with_3_cards"
                                ]["text"],
                                server_id=server.id,
                            )
                        )
                    else:
                        set_state(server.id, VP_CARD_CHOICE)

                        await ctx.send(
                            embed=get_embed(
                                title=SERVER_ON_CEO_ASSIGNMENT_SUCCESS[
                                    "title"
                                ],
                                description=SERVER_ON_CEO_ASSIGNMENT_SUCCESS[
                                    "text"
                                ].format(mention=f"<@{ceo_candidate.id}>"),
                                server_id=server.id,
                            )
                        )

                        for _ in range(3):
                            add_random_card(server.id)

                        card1 = get_card(server.id, 0)
                        card2 = get_card(server.id, 1)
                        card3 = get_card(server.id, 2)

                        img_name = (
                            card1.value[0] + card2.value[0] + card3.value[0]
                        )

                        vp = get_vp(server.id)
                        await send_dm(
                            bot=bot,
                            players_id_list=[vp.id],
                            title=DM_ON_CEO_ASSIGNMENT_SUCCESS["title"],
                            description=DM_ON_CEO_ASSIGNMENT_SUCCESS["text"],
                            img_name=img_name,
                        )

                        set_state(server.id, VP_CARD_CHOICE)

                else:
                    await ctx.send(
                        embed=get_embed(
                            title=SERVER_ON_CEO_ASSIGNMENT_FAILURE["title"],
                            description=SERVER_ON_CEO_ASSIGNMENT_FAILURE[
                                "text"
                            ].format(mention=f"<@{ceo_candidate.id}>"),
                            server_id=server.id,
                        )
                    )
                    await start_round(bot, server)


async def choose_vp(ctx, mention):
    server = ctx.guild
    vp = get_vp(server.id)

    if "@" not in [mention[0], mention[1]]:
        description = SERVER_CHOOSE_CANDIDATE_COMMAND["no_at_sign_error_text"]
    else:
        mentioned_player_id = get_id_from_discord_mention(mention)
        players_id_list = get_players_id_list(server.id)

        if mentioned_player_id not in players_id_list:
            description = SERVER_CHOOSE_CANDIDATE_COMMAND[
                "player_not_found_error_text"
            ]
        elif mentioned_player_id == vp.id:
            description = SERVER_CHOOSE_CANDIDATE_COMMAND[
                "self_assign_error_text"
            ]
        else:
            set_ceo_candidate(mentioned_player_id)
            set_state(server.id, CEO_CANDIDATE_VOTING_STATE)
            set_vote(server.id, vp.id, True)
            description = SERVER_CHOOSE_CANDIDATE_COMMAND["text"].format(
                mention=mention
            )

    await ctx.send(
        embed=get_embed(
            title=SERVER_CHOOSE_CANDIDATE_COMMAND["title"],
            description=description,
            server_id=server.id,
        )
    )


async def vp_choose_card(bot, server, index):
    index = get_index(index)

    if index:
        delete_card(server.id, index - 1)
        set_state(server.id, CEO_CARD_CHOICE)

        vp = get_vp(server.id)
        await send_dm(
            bot=bot,
            players_id_list=[vp.id],
            title=DM_ON_CHOOSE_CARD_COMMAND["title"],
            description=DM_ON_CHOOSE_CARD_COMMAND["text"],
        )

        card1 = get_card(server.id, 0)
        card2 = get_card(server.id, 1)
        img_name = card1.value[0] + card2.value[0]

        ceo = get_ceo(server.id)
        await send_dm(
            bot=bot,
            players_id_list=[ceo.id],
            title=DM_ON_VP_CHOOSE_CARD_COMMAND_SUCCESS["title"],
            description=DM_ON_VP_CHOOSE_CARD_COMMAND_SUCCESS["text"],
            img_name=img_name,
        )

        current_guild = bot.get_guild(server.id)
        channel = await get_channel(current_guild)
        await channel.send(
            embed=get_embed(
                title=SERVER_ON_VP_CHOOSE_CARD_COMMAND_SUCCESS["title"],
                description=SERVER_ON_VP_CHOOSE_CARD_COMMAND_SUCCESS["text"],
                server_id=server.id,
            )
        )

    else:
        vp = get_vp(server.id)
        await send_dm(
            bot=bot,
            players_id_list=[vp.id],
            title=DM_ON_CHOOSE_CARD_COMMAND["title"],
            description=DM_ON_CHOOSE_CARD_COMMAND["card_not_found_error_text"],
        )


async def ceo_choose_card(bot, server, index):
    index = get_index(index)

    if index:
        delete_card(server.id, index - 1)

        ceo = get_ceo(server.id)
        await send_dm(
            bot=bot,
            players_id_list=[ceo.id],
            title=DM_ON_CHOOSE_CARD_COMMAND["title"],
            description=DM_ON_CHOOSE_CARD_COMMAND["text"],
        )

        card1 = get_card(server.id, 0)
        img_name = card1.value[0]

        current_guild = bot.get_guild(server.id)
        channel = await get_channel(current_guild)
        file = discord.File(
            os.path.join(
                os.getcwd(),
                APP_FOLDER_NAME,
                IMG_FOLDER_NAME,
                CARD_COMBINATIONS_FOLDER_NAME,
                f"{img_name}.png",
            ),
            filename=f"{img_name}.png",
        )
        await channel.send(
            file=file,
            embed=get_embed(
                title=SERVER_ON_CEO_CHOOSE_CARD_COMMAND_SUCCESS["title"],
                description=SERVER_ON_CEO_CHOOSE_CARD_COMMAND_SUCCESS["text"],
                img_name=f"{img_name}.png",
                server_id=server.id,
            ),
        )

        await end_round(bot, server, img_name)

    else:
        ceo = get_ceo(server.id)
        await send_dm(
            bot=bot,
            players_id_list=[ceo.id],
            title=DM_ON_CHOOSE_CARD_COMMAND["title"],
            description=DM_ON_CHOOSE_CARD_COMMAND["card_not_found_error_text"],
        )


class RoundProcessing(commands.Cog):
    """
    Class that processes bot commands while game session is running

    List of commands to process:

    choose @nickname - choose player to set "ceo-candidate" character
    choose num - choose card to delete from the list of session cards
    iYes - vote positively while ceo-candidate elections
    iNo - vote negatively while ceo-candidate elections
    """

    def __init__(self, bot):
        self.bot = bot

    @commands.command()
    async def choose(self, ctx, arg):
        msg_author = ctx.message.author

        if ctx.guild:
            if ctx.message.channel.name == TEXT_CHANNEL_NAME:
                server = ctx.guild
            else:
                await ctx.send(SERVER_ON_WRONG_CHANNEL_TEXT)
                return None
        else:
            server = get_server_by_player_id(msg_author.id)

        vp = get_vp(server.id)
        ceo = get_ceo(server.id)

        if get_state(server.id) == VP_CEO_CHOICE_STATE:
            if vp and msg_author.id == vp.id:
                await choose_vp(ctx, arg)

        elif get_state(server.id) == VP_CARD_CHOICE:
            if vp and msg_author.id == vp.id:
                await vp_choose_card(self.bot, server, arg)

        elif get_state(server.id) == CEO_CARD_CHOICE:
            if ceo and msg_author.id == ceo.id:
                await ceo_choose_card(self.bot, server, arg)

    @commands.command(name="iYes")
    async def yes(self, ctx):
        if ctx.guild:
            if ctx.message.channel.name == TEXT_CHANNEL_NAME:
                await process_vote_ans(ctx, self.bot, True)
            else:
                await ctx.send(SERVER_ON_WRONG_CHANNEL_TEXT)

    @commands.command(name="iNo")
    async def no(self, ctx):
        if ctx.guild:
            if ctx.message.channel.name == TEXT_CHANNEL_NAME:
                await process_vote_ans(ctx, self.bot, False)
            else:
                await ctx.send(SERVER_ON_WRONG_CHANNEL_TEXT)
