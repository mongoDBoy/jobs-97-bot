"""
This module processes general bot commands
"""

from discord.ext import commands

from jobs_97_bot.config import (
    DISCORD_CLIENT_ID,
    DISCORD_PERMISSIONS,
    DISCORD_SCOPE,
    TEXT_CHANNEL_NAME,
)
from jobs_97_bot.db.db_utils import get_channel, remove_session, reset_session
from jobs_97_bot.utils.strings import (
    DM_ADD_COMMAND_TEXT,
    DM_HELP_COMMAND,
    ON_DM_COMMAND_ON_SERVER_TEXT,
    ON_JOINED_TO_SERVER_TEXT,
    RULES_COMMAND,
    SERVER_HELP_COMMAND,
    SERVER_ON_WRONG_CHANNEL_TEXT,
)
from jobs_97_bot.utils.utils import get_embed


class General(commands.Cog):
    """
    Class that processes general bot commands:

    List of commands to process:

    help - shows list of available commands
    rules - shows game rules
    add - gives link, which adds bot to the server
    """

    def __init__(self, bot):
        self.bot = bot

    @commands.Cog.listener()
    async def on_guild_join(self, guild):
        reset_session(guild.id)
        channel = await get_channel(guild)
        await channel.send(ON_JOINED_TO_SERVER_TEXT)

    @commands.Cog.listener()
    async def on_guild_remove(self, guild):
        remove_session(guild.id)

    @commands.command()
    async def help(self, ctx):
        if ctx.guild:
            if ctx.message.channel.name == TEXT_CHANNEL_NAME:
                await ctx.send(
                    embed=get_embed(
                        title=SERVER_HELP_COMMAND["title"],
                        description=SERVER_HELP_COMMAND["text"],
                    )
                )
            else:
                await ctx.send(SERVER_ON_WRONG_CHANNEL_TEXT)
        else:
            await ctx.send(
                embed=get_embed(
                    title=DM_HELP_COMMAND["title"],
                    description=DM_HELP_COMMAND["text"],
                )
            )

    @commands.command()
    async def rules(self, ctx):
        if not ctx.guild or ctx.message.channel.name == TEXT_CHANNEL_NAME:
            await ctx.send(
                embed=get_embed(paragraphs=RULES_COMMAND["paragraphs"])
            )
        else:
            await ctx.send(SERVER_ON_WRONG_CHANNEL_TEXT)

    @commands.command()
    async def add(self, ctx):
        if ctx.guild:
            if ctx.message.channel.name == TEXT_CHANNEL_NAME:
                await ctx.send(ON_DM_COMMAND_ON_SERVER_TEXT)
            else:
                await ctx.send(SERVER_ON_WRONG_CHANNEL_TEXT)
        else:
            await ctx.send(
                DM_ADD_COMMAND_TEXT.format(
                    client_id=DISCORD_CLIENT_ID,
                    permissions=DISCORD_PERMISSIONS,
                    scope=DISCORD_SCOPE,
                )
            )
